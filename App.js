import React, { Component } from "react";
import { Text, View } from "react-native";
import axios from "axios";
import config from "react-native-config";
import VersionNumber from "react-native-version-number";

export default class App extends Component {
  state = {
    text: "hi"
  };
  async componentDidMount() {
    console.log(config.API_URL);
    let res = await axios.get(`${config.API_URL}/dev`);
    console.log(res);
    await this.setState({
      text: res.data.msg
    });
  }
  render() {
    return (
      <View>
        <Text>{this.state.text}</Text>
        <Text>{VersionNumber.appVersion}</Text>
        <Text>{VersionNumber.buildVersion}</Text>
        <Text>{VersionNumber.bundleIdentifier}</Text>
        <Text>{config.API_URL}</Text>
      </View>
    );
  }
}
